//
// Created by HZY on 2023/2/25.
//
#include <cstdio>
#include <cstring>
#include <iostream>

#include "xxapi.h"
#include "base64.h"
#include "xxtea.h"

#ifndef NDEBUG

jstring JNICALL JNI_FUNC(_1en)(JNIEnv *env, jclass clazz, jstring _txt, jstring _pwd) {
    auto txt = env->GetStringUTFChars(_txt, nullptr);
    auto pwd = env->GetStringUTFChars(_pwd, nullptr);
    size_t len;
    auto *encrypt_data = xxtea_encrypt(txt, strlen(txt), pwd, &len);
    char *base64_data = base64_encode((unsigned char *) encrypt_data, len);
    std::string ret = pwd;
    ret.append(base64_data);
    free(encrypt_data);
    free(base64_data);
    env->ReleaseStringUTFChars(_txt, txt);
    env->ReleaseStringUTFChars(_pwd, pwd);
    return env->NewStringUTF(ret.c_str());
}

#endif

jstring JNICALL JNI_FUNC(_1de)(JNIEnv *env, jclass clazz, jstring _txt) {
    auto txt = env->GetStringUTFChars(_txt, nullptr);
    std::string txt_str = txt;
    auto pwd = txt_str.substr(0, 4);
    auto cpt = txt_str.substr(4, txt_str.length() - 4);
    env->ReleaseStringUTFChars(_txt, txt);
    size_t len = 0;
    unsigned char *cpt_data = base64_decode(cpt.c_str(), &len);
    void *decrypt_data = xxtea_decrypt(cpt_data, len, pwd.c_str(), &len);
    auto ret = env->NewStringUTF((char *) decrypt_data);
    free(cpt_data);
    free(decrypt_data);
    return ret;
}