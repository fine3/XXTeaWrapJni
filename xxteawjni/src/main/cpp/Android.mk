LOCAL_PATH := $(call my-dir)

# build shared libs
include $(CLEAR_VARS)
LOCAL_MODULE := OqgCsKPtdGYWy

LOCAL_C_INCLUDES := \

LOCAL_SRC_FILES := \
	$(wildcard $(LOCAL_PATH)/*.cpp) \
	$(wildcard $(LOCAL_PATH)/*.c) \

LOCAL_CFLAGS += -ffunction-sections -fdata-sections
LOCAL_CFLAGS += -fvisibility=hidden -fexceptions
LOCAL_LDFLAGS += -Wall,--gc-sections

LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)