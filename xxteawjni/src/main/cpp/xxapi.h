//
// Created by HZY on 2023/2/25.
//

#ifndef XXTEAWRAPJNI_XXAPI_H
#define XXTEAWRAPJNI_XXAPI_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

#define JNI_FUNC(x) Java_com_hzy_xxteaw_De_##x

#ifndef NDEBUG

JNIEXPORT jstring JNICALL
JNI_FUNC(_1en)(JNIEnv *env, jclass clazz, jstring txt, jstring pwd);
    
#endif

JNIEXPORT jstring JNICALL
JNI_FUNC(_1de)(JNIEnv *env, jclass clazz, jstring txt);

#ifdef __cplusplus
}
#endif

#endif //XXTEAWRAPJNI_XXAPI_H
