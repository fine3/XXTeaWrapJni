package com.demo.fine.guard;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hzy.xxteaw.De;

public class MainActivity extends AppCompatActivity {

    private TextView mTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String ori = "Adobe Acrobat 支持各种针对电子和数字签名的解决方案。这些解决方案包含允许您使用基于证书的\" + \"数字 ID 对 PDF 文件进行签名的证书签名。证书签名也称为数字签名。在 Acrobat 中，您可以创建自\" + \"己的证书 ID。然而，更常见的方法是使用受信任的第三方证书颁发机构颁发的证书 ID。Acrobat 中的\" + \"其他签名选项包括与 Adobe Acrobat Sign 的集成。根据某些司法辖区的法律规定，您可能有权：向我们\" + \"索要您的个人信息的副本；更正、删除或限制（阻止任何常规的处理活动）处理您的个人信息；获取您为某一合同\" + \"而向我们提供的个人信息，或以结构清晰且机器可读的形式表示同意，向我们提供的个人信息；以及要求我们将此信息\" + \"与其他控制方共享（转移给其他控制方）。根据您所在司法管辖区适用的数据隐私法，您可能有权获得额外权利。";
        String end = De.en(ori, "yu&6");
        String de = De.de(end);
        mTv = findViewById(R.id.id_temp_text);
        mTv.setText(de);
    }
}
